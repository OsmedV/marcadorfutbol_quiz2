import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { stringify } from 'querystring';
import { productos, proveedor } from './products.model';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private proveedore: proveedor[] = [
    {
      nombre: "ESPN2, FOX SPORT y TELETICA",
      codigo: 222
    }
  ];
  private productos:productos [] = [
    {
      proveedor: this.proveedore,
      codigo: 12,
      nombre: "LDA VS CSH",
      marcador: "(1) - (0)",
      fecha_partido: "20/12/2020",
      descripcion: "Gol al minuto 15 Jonatan Moya.\n ENTRA minuto 16 keyner Brown. Sale Christian Reyes.\n Tarjeta amarilla minuto 17 Grerson Torres\n ENTRA minuto 20 Alvaro saborio SALE Montenegro"
    },
    {
      proveedor: this.proveedore,
      codigo: 13,
      nombre: "Villa Real vs Osasuna",
      marcador: "(3) - (1)",
      fecha_partido: "17/09/2020",
      descripcion: "Gol al minuto 7 gerald moreno\nTajeta roja  minuto 17 Aridane hernandez\n Gol al minuto 29 fernando niño\n  --------------MEDIO TIEMPO------------\n  Gol (P) minuto 70 Roberto torres\n  Gol al minuto 86 gerald moreno\n"
    },
    {
      proveedor: this.proveedore,
      codigo: 14,
      nombre: "Schalke 04 vs Augsburgo FC",
      marcador: "(2) - (2)",
      fecha_partido: "25/10/2020",
      descripcion: "autogol al minuto 32 Suat Sedar\n -----------Medio tiempo-------------\n gol al minuto 52 Benito Raman\n tarjeta roja al minuto 53 Florian Niederlechner\n gol al minuto 61 Nassim Boujellab\n gol al minuto 90+3 Marco Richter"
    },
    {
      proveedor: this.proveedore,
      codigo: 15,
      nombre: "union berlin vs bayern münchen",
      marcador: "(1) - (1)",
      fecha_partido: "22/12/2020",
      descripcion: "gol al minuto 4 Grischa Prömel\n -------------Medio tiempo-------\n gol al minuto 67 Robert Lewandowski\n"}
  ];
  constructor(
    private http: HttpClient
  ) { }
  getAll(){
    return [...this.productos];
  }
  getProduct(productId: number){
    return {
      ...this.productos.find(
        product => {
          return product.codigo === productId;
        }
      )
    };
  }
  deleteProduct(productId: number){
    this.productos = this.productos.filter(
      product => {
        return product.codigo !== productId;
      }
    );
  }
  addProduct(
    pcodigo: number,
    pnombre: string,
    pmarcador: string,
    pfecha_partido: string,
    pdescripcion: string){

      const product: productos = {
        proveedor: this.proveedore,
        codigo: pcodigo,
        nombre: pnombre,
        marcador: pmarcador,
        fecha_partido: pfecha_partido,
        descripcion: pdescripcion
      }
      this.http.post('https://clasesmiercoles-49dde.firebaseio.com/addProduct.json',
        { 
          ...product, 
          id:null
        }).subscribe(() => {
          console.log('entro');
        });
      this.productos.push(product);
      
  }
  UpdateProduct(
    pnombre: string,
    pcodigo: number,
    pmarcador: string,
    pdescripcion: string
    ){
    let index = this.productos.map((x) => x.codigo).indexOf(pcodigo);
    
    this.productos[index].nombre = pnombre;
    this.productos[index].codigo = pcodigo;
    this.productos[index].marcador = pmarcador;
    this.productos[index].descripcion = pdescripcion;

    console.log(this.productos);
  }
}
